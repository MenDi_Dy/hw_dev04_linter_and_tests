import aiohttp_jinja2
import asyncpg
from aiohttp import web

from model.NLP_model import nlp_model
from vk.vk_pars import vk_posts


@aiohttp_jinja2.template('index.html')
async def handler(request):
    """Инициализирует html форму."""
    return {}


@aiohttp_jinja2.template('result.html')
async def predict(request) -> dict:
    """Возвращает html форму с сентиментами для поста."""
    data = await request.post()
    group_link = data['Group_link']
    count = int(data['Post_count'])
    post = vk_posts(group_link, count)
    res = nlp_model(group_link, count)
    str_sen = str(", ".join(map(str, res)))
    conn = await asyncpg.connect(user='user', password='password',
                                 database='vk_post', host='127.0.0.1')

    await conn.execute('''
                       INSERT INTO vk_sen VALUES($1, $2, $3) 
                       ''', group_link, count, str_sen)

    await conn.close()
    text = ({
        'Group_link': group_link,
        'count': count,
        'post': post,
        'res': res,
    })
    return text


async def version(request: web.Request) -> web.Response:
    """Возвращает номер версии сервиса."""
    config = request.app['config']
    return web.Response(text=config['service']['version'])
